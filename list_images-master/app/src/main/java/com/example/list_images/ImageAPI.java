package com.example.list_images;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ImageAPI {
    public static final String BASE_URL = "http://172.20.217.108/retrofit/";
    @GET("list_images.php")
    Call<List<ImagesList>> getImages();
}
