package com.example.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MyInterface {
    public static final String BASE_URL = "https://itlearningcenters.com/android/project0309/";
    @GET("list_shops.php")
    Call<List<Customer>> getCustomers();
    // Customer is POJO class to get the data from API,
    // use List<UserListResponse> in callback
    // because the data in our API is starting from JSONArray
}
