package com.example.retrofit;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listViewCustomers);
        //defining a progress dialog to show while loading
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loadind Data...");
        progressDialog.show();
        // 1. Building retrofit object
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MyInterface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        // 2. Defining retrofit api service
        MyInterface myInterface = retrofit.create(MyInterface.class);
        // 3. Defining the call
        Call<List<Customer>> call = myInterface.getCustomers();
        // 4. Calling the api as synchronous request
        //Create a retrofit2.Callback object and
        //override it’s onResponse and onFailure method.
        call.enqueue(new Callback<List<Customer>>() {
            @Override
            public void onResponse(Call<List<Customer>> call,
                                   Response<List<Customer>> response) {
                //hiding progress dialog
                progressDialog.dismiss();
                //5. Converted JSON result stored in response.body()
                // and put response data to customer list
                List<Customer> customersList = response.body();
                //6. Creating an String array for the ListView
                String[] customers = new String[customersList.size()];
                //7. looping through all the customers and inserting the names inside the string array
                for (int i = 0; i < customersList.size(); i++) {
                    customers[i] = customersList.get(i).getUsername();
                }
                //8. displaying the string array into listview
                listView.setAdapter(new
                        ArrayAdapter<String>(getApplicationContext(), R.layout.listrow, R.id.tvList,
                        customers));
            }
            @Override
            public void onFailure(Call<List<Customer>> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }
}